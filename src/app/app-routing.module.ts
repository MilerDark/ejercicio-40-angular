import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VeterinariaComponent } from './components/veterinaria/veterinaria.component';

const routes: Routes = [
  {path: 'reactivo', component: VeterinariaComponent},
  {path: '**', pathMatch: 'full', redirectTo:'reactivo'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
