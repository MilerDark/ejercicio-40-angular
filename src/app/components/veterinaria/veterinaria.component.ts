import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/servicios/validadores.service';

@Component({
  selector: 'app-veterinaria',
  templateUrl: './veterinaria.component.html',
  styleUrls: ['./veterinaria.component.css']
})
export class VeterinariaComponent implements OnInit {
  forma!: FormGroup;
  constructor(private fb: FormBuilder,
              private validadores: ValidadoresService) { 
                this.crearFormulario();
              }
  ngOnInit(): void {
  }

  get nombreNoValido(){
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }

  // get nombreValido(){
  //   return this.forma.get('nombre')?.valid && this.forma.get('nombre')?.touched;
  // }

  get edadNoValido(){
    return this.forma.get('edad')?.invalid && this.forma.get('edad')?.touched;
  }

  get especieNoValido(){
    return this.forma.get('especie')?.invalid && this.forma.get('especie')?.touched;
  }

  get observacionesNoValido(){
    return this.forma.get('observaciones')?.invalid && this.forma.get('observaciones')?.touched;
  }


  crearFormulario():void{
    this.forma = this.fb.group({
      nombre: ['', Validators.required],
      edad: ['', Validators.required],
      especie: ['', Validators.required],
      observaciones: ['', Validators.required]
    })
  }

  guardar():void{
    console.log(this.forma.value);

    this.limpiarFormulario();
  }

  limpiarFormulario(){
    this.forma.reset
  }
}
